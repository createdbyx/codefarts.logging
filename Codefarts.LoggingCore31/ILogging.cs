﻿// <copyright>
//   Copyright (c) 2012 Codefarts
//   All rights reserved.
//   contact@codefarts.com
//   http://www.codefarts.com
// </copyright>

namespace Codefarts.Logging
{
    /// <summary>
    /// Provides a logging interface.
    /// </summary>
    public interface ILogging
    {
        /// <summary>
        /// Logs a simple message to the log.
        /// </summary>
        /// <param name="message">The message to be logged.</param>
        void Log(string message);

        /// <summary>
        /// Logs a simple message to the log.
        /// </summary>
        /// <param name="type">The type of message to be logged.</param>
        /// <param name="message">The message to be logged.</param>
        void Log(LogEntryType type, string message);

        /// <summary>
        /// Logs a simple message to the log.
        /// </summary>
        /// <param name="message">The message to be logged.</param>
        /// <param name="category">The category associated with the log.</param>
        void Log(string message, string category);

        /// <summary>
        /// Logs a simple message to the log.
        /// </summary>
        /// <param name="type">The type of message to be logged.</param>
        /// <param name="message">The message to be logged.</param>
        /// <param name="category">The category associated with the log.</param>
        void Log(LogEntryType type, string message, string category);   
    }
}
