﻿// <copyright>
//   Copyright (c) 2012 Codefarts
//   All rights reserved.
//   contact@codefarts.com
//   http://www.codefarts.com
// </copyright>

namespace Codefarts.Logging
{
    using System;

    /// <summary>
    ///     Provides event arguments for logging events.
    /// </summary>
    public class LogModelEventArgs : EventArgs
    {
        #region Constructors and Destructors

        /// <summary>
        /// Initializes a new instance of the <see cref="LogModelEventArgs"/> class.
        /// </summary>
        /// <param name="type">
        /// The type.
        /// </param>
        /// <param name="category">
        /// The category.
        /// </param>
        /// <param name="message">
        /// The message.
        /// </param>
        public LogModelEventArgs(LogEntryType type, string category, string message)
        {
            this.Type = type;
            this.Category = category;
            this.Message = message;
            this.TimeStamp = DateTime.Now;
        }

        /// <summary>Initializes a new instance of the <see cref="LogModelEventArgs" /> class.</summary>
        public LogModelEventArgs()
        {
        }

        /// <summary>
        /// Initializes a new instance of the <see cref="LogModelEventArgs"/> class.
        /// </summary>
        /// <param name="timeStamp">
        /// The time stamp.
        /// </param>
        /// <param name="type">
        /// The type.
        /// </param>
        /// <param name="category">
        /// The category.
        /// </param>
        /// <param name="message">
        /// The message.
        /// </param>
        public LogModelEventArgs(DateTime timeStamp, LogEntryType type, string category, string message)
        {
            this.TimeStamp = timeStamp;
            this.Type = type;
            this.Category = category;
            this.Message = message;
        }

        #endregion

        #region Public Properties

        /// <summary>Gets or sets the category.</summary>
        /// <value>The category.</value>
        public string Category { get; set; }

        /// <summary>Gets or sets the message.</summary>
        /// <value>The message.</value>
        public string Message { get; set; }

        /// <summary>Gets or sets the time stamp.</summary>
        /// <value>The time stamp.</value>
        public DateTime TimeStamp { get; set; }

        /// <summary>Gets or sets the type.</summary>
        /// <value>The type.</value>
        public LogEntryType Type { get; set; }

        #endregion
    }
}