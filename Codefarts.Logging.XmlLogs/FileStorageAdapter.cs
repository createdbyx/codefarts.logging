﻿// <copyright>
//   Copyright (c) 2012 Codefarts
//   All rights reserved.
//   contact@codefarts.com
//   http://www.codefarts.com
// </copyright>         

namespace Codefarts.Logging.XmlLogs
{
    using System;
    using System.Collections.Generic;
    using System.Diagnostics;
    using System.IO;
    using System.Threading;
    using System.Threading.Tasks;
    using System.Xml.Serialization;

    /// <summary>
    /// The file storage adapter.
    /// </summary>
    public static class FileStorageAdapter
    {
        #region Static Fields

        /// <summary>Holds a dictionary of semaphores to assist with threading.</summary>
        private static readonly Dictionary<string, SemaphoreSlim> Semaphores = new Dictionary<string, SemaphoreSlim>();

        #endregion

        #region Public Methods and Operators

        /// <summary>
        /// Loads the data from a file.
        /// </summary>
        /// <typeparam name="T">
        /// The type of object to de-serialize.
        /// </typeparam>
        /// <param name="filename">
        /// The filename where the data is to be read from.
        /// </param>
        /// <returns>
        /// A new de-serialized object of type T.
        /// </returns>
        public static async Task<T> LoadData<T>(string filename)
        {
            var semaphore = GetSemaphore(filename);
            await semaphore.WaitAsync();

            try
            {
                using (var storageFile = new FileStream(filename, FileMode.Open, FileAccess.Read, FileShare.None, 8192))
                {
                    using (var stream = new MemoryStream())
                    {
                        await storageFile.CopyToAsync(stream);
                        stream.Seek(0, SeekOrigin.Begin);

                        var serializer = new XmlSerializer(typeof(T));
                        return (T)serializer.Deserialize(stream);
                    }
                }
            }
            finally
            {
                semaphore.Release();
            }
        }

        /// <summary>
        /// Saves the data to a file.
        /// </summary>
        /// <typeparam name="T">
        /// The type of object to de-serialize.
        /// </typeparam>
        /// <param name="data">
        /// The data to be serialized.
        /// </param>
        /// <param name="filename">
        /// The filename where the data will be saved to.
        /// </param>
        /// <returns>
        /// Returns a Task object for asynchronous code.
        /// </returns>
        public static async Task SaveData<T>(T data, string filename, bool? finalizing)
        {
            var semaphore = GetSemaphore(filename);
            await semaphore.WaitAsync();

            try
            {
                Debug.Indent();
                Debug.WriteLine("XMLlogging SaveData start ");
                using (var stream = new MemoryStream())
                {
                    var serializer = new XmlSerializer(typeof(T));
                    serializer.Serialize(stream, data);
                    Debug.WriteLine("XMLlogging SaveData length " + stream.Length);

                    stream.Seek(0, SeekOrigin.Begin);

                    using (var storageFile = new FileStream(filename, FileMode.OpenOrCreate, FileAccess.Write, FileShare.None, 8196))
                    {
                        //if (finalizing.HasValue && finalizing.Value)
                        //{
                        //    stream.CopyTo(storageFile);
                        //}
                        //else
                        //{
                            await stream.CopyToAsync(storageFile);
                        //}
                    }
                }

                //using (var storageFile = new FileStream(filename, FileMode.Create, FileAccess.Write, FileShare.None, 8196, true))
                //{
                //    var serializer = new XmlSerializer(typeof(T));
                //    serializer.Serialize(storageFile, data);
                //    Debug.WriteLine("XMLlogging SaveData length " + storageFile.Length);
                //}

                Debug.WriteLine("XMLlogging SaveData complete ");
            }
            catch (Exception ex)
            {
                Debug.WriteLine("XMLlogging SaveData exception caught ");
                Debug.WriteLine(ex.Message);
            }
            finally
            {
                Debug.Unindent();
                semaphore.Release();
            }
        }

        #endregion

        #region Methods

        /// <summary>
        /// Gets the semaphore for a given path and filename.
        /// </summary>
        /// <param name="filename">
        /// The filename to retrieve the semaphore for.
        /// </param>
        /// <returns>
        /// Returns a semaphore object associated with the filename.
        /// </returns>
        internal static SemaphoreSlim GetSemaphore(string filename)
        {
            if (Semaphores.ContainsKey(filename))
            {
                return Semaphores[filename];
            }

            var semaphore = new SemaphoreSlim(1);
            Semaphores[filename] = semaphore;
            return semaphore;
        }

        #endregion
    }
}