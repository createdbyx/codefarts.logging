﻿// <copyright>
//   Copyright (c) 2012 Codefarts
//   All rights reserved.
//   contact@codefarts.com
//   http://www.codefarts.com
// </copyright>

namespace Codefarts.Logging.XmlLogs
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.Composition;
    using System.IO;
    using System.Threading.Tasks;

    using Codefarts.Settings;

    /// <summary>
    ///     Provides a xml file based log.
    /// </summary>
    [Export(typeof(ILogging))]
    public class XmlLogging : ILogging
    {
        #region Fields
                                                  
        /// <summary>
        ///     Holds a list of log entries.
        /// </summary>
        private readonly List<LogModel> model = new List<LogModel>();
                           
        /// <summary>
        ///     Holds the name of the active log filename.
        /// </summary>
        private string logFile;

        private string fileName;

        #endregion

        #region Constructors and Destructors

        /// <summary>Initializes a new instance of the <see cref="XmlLogging" /> class.</summary>
        public XmlLogging()
        {
            this.NewLogFile();
        }

        /// <summary>Finalizes an instance of the <see cref="XmlLogging"/> class.</summary>
        ~XmlLogging()
        {
            this.DoSave(true, true);
        }

        #endregion

        #region Public Methods and Operators

        /// <summary>
        /// Logs a simple message to the log.
        /// </summary>
        /// <param name="message">
        /// The message to be logged.
        /// </param>
        public void Log(string message)
        {
            this.Log(LogEntryType.Generic, message, null);
        }

        /// <summary>
        /// Logs a simple message to the log.
        /// </summary>
        /// <param name="type">
        /// The type of message to be logged.
        /// </param>
        /// <param name="message">
        /// The message to be logged.
        /// </param>
        public void Log(LogEntryType type, string message)
        {
            this.Log(type, message, null);
        }

        /// <summary>
        /// Logs a simple message to the log.
        /// </summary>
        /// <param name="message">
        /// The message to be logged.
        /// </param>
        /// <param name="category">
        /// The category associated with the log.
        /// </param>
        public void Log(string message, string category)
        {
            this.Log(LogEntryType.Generic, message, category);
        }

        /// <summary>
        /// Logs a simple message to the log.
        /// </summary>
        /// <param name="type">
        /// The type of message to be logged.
        /// </param>
        /// <param name="message">
        /// The message to be logged.
        /// </param>
        /// <param name="category">
        /// The category associated with the log.
        /// </param>
        public async void Log(LogEntryType type, string message, string category)
        {
            var model = new LogModel();
            model.Category = category;
            model.Message = message;
            model.TimeStamp = DateTime.Now;
            model.Type = type;
            model.Id = this.model.Count;

            this.model.Add(model);
            await this.DoSave(false, false);
        }

        #endregion

        #region Methods
        
        /// <summary>Saves the current list of log entries.</summary>
        /// <returns>Returns a Task object for asynchronous code.</returns>
        private async Task DoSave(bool flush, bool? finalizing)
        {
            if (flush == false && this.model.Count < 8192)
            {
                return;
            }

            if (string.IsNullOrWhiteSpace(this.fileName))
            {
                var dataFolder = SettingsManager.Instance.GetSetting(SettingConstants.LogFolder, AppDomain.CurrentDomain.BaseDirectory);
                this.fileName = Path.Combine(dataFolder, this.logFile);
            }

            await FileStorageAdapter.SaveData(this.model, this.fileName, finalizing);

            if (File.Exists(this.fileName))
            {
                // check if existing file size beyond our 5Mb limit and start a new one if it is
                var info = new FileInfo(this.fileName);
                if (info.Length > 1048576 * 5)
                {
                    this.NewLogFile();
                    this.fileName = Path.Combine(Path.GetDirectoryName(this.fileName), this.logFile);
                    this.model.Clear();
                }
            }
        }
                 
        /// <summary>
        ///     Starts a new log file name.
        /// </summary>
        private void NewLogFile()
        {
            this.logFile = string.Format("{0}.log", DateTime.Now.ToString(@"M-d-yyyy hh-mm-ss tt"));
        }

        #endregion
    }
}