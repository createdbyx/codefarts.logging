﻿// <copyright>
//   Copyright (c) 2012 Codefarts
//   All rights reserved.
//   contact@codefarts.com
//   http://www.codefarts.com
// </copyright>

namespace Codefarts.Logging.WinFormControls
{
    using System;
    using System.Windows.Forms;

    using Codefarts.Logging;

    // public delegate void GenericCallback<T>(T e);

    public class TextBoxLogger : ILogging
    {
        /// <summary>
        /// Initializes a new instance of the <see cref="TextBoxLogger"/> class.
        /// </summary>
        /// <param name="textBox">
        /// The text box.
        /// </param>
        public TextBoxLogger(TextBox textBox)
        {
            this.TextBox = textBox;
        }

        public bool AutoScroll { get; set; }

        public TextBox TextBox { get; set; }

        /// <summary>
        /// Initializes a new instance of the <see cref="TextBoxLogger"/> class.
        /// </summary>
        public TextBoxLogger()
        {
        }

        public void Log(string message)
        {
            this.Log(LogEntryType.Generic, message, null);
        }

        public void Log(LogEntryType type, string message)
        {
            this.Log(type, message, null);
        }

        public void Log(string message, string category)
        {
            this.Log(LogEntryType.Generic, message, category);
        }

        public void Log(LogEntryType type, string message, string category)
        {
            var action = new Action<TextBox>(
                tb =>
                {
                    if (this.AutoScroll)
                    {
                        tb.SelectionStart = this.TextBox.TextLength - 1;
                        tb.SelectionLength = 0;
                        tb.ScrollToCaret();
                    }
                });

            this.AddLogText(string.Format("\r\n{0}: ({1}) {2} -> {3}", DateTime.Now, category, type, message));
        }

        private void AddLogText(string text)
        {
            try
            {
                if (this.TextBox.InvokeRequired)
                {
                    //var d = new SetTextCallback(AddLogText);
                    var d = new Action<string>(this.AddLogText);
                    this.TextBox.Invoke(d, new object[] { text });
                }
                else
                {
                    this.TextBox.Text += text;
                }
            }
            catch (ObjectDisposedException ode)
            {

            }
        }

    }
}