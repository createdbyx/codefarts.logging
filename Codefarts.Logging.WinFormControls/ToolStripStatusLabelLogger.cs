﻿// <copyright>
//   Copyright (c) 2012 Codefarts
//   All rights reserved.
//   contact@codefarts.com
//   http://www.codefarts.com
// </copyright>

namespace Codefarts.Logging.WinFormControls
{
    using System;
    using System.Windows.Forms;

    using Codefarts.Logging;

    public class ToolStripStatusLabelLogger : ILogging
    {
        /// <summary>
        /// Initializes a new instance of the <see cref="ToolStripStatusLabelLogger"/> class.
        /// </summary>
        /// <param name="label">
        /// The text box.
        /// </param>
        public ToolStripStatusLabelLogger(ToolStripStatusLabel label)
        {
            this.Label = label;
        }

        public ToolStripStatusLabel Label { get; set; }

        /// <summary>
        /// Initializes a new instance of the <see cref="ToolStripStatusLabelLogger"/> class.
        /// </summary>
        public ToolStripStatusLabelLogger()
        {
        }

        public void Log(string message)
        {
            this.Log(LogEntryType.Generic, message, null);
        }

        public void Log(LogEntryType type, string message)
        {
            this.Log(type, message, null);
        }

        public void Log(string message, string category)
        {
            this.Log(LogEntryType.Generic, message, category);
        }

        public void Log(LogEntryType type, string message, string category)
        {
            this.AddLogText(string.Format("{0}: ({1}) {2} -> {3}", DateTime.Now, category, type, message));
        }

        private void AddLogText(string text)
        {
            try
            {
                if (this.Label.GetCurrentParent().InvokeRequired)
                {
                    //var d = new SetTextCallback(AddLogText);
                    var d = new Action<string>(this.AddLogText);
                    this.Label.GetCurrentParent().Invoke(d, new object[] { text });

                }
                else
                {
                    this.Label.Text = text;
                }
            }
            catch (ObjectDisposedException ode)
            {

            }
        }

    }
}