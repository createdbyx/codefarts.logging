﻿// <copyright>
//   Copyright (c) 2012 Codefarts
//   All rights reserved.
//   contact@codefarts.com
//   http://www.codefarts.com
// </copyright>

namespace Codefarts.Logging.XmlTraceListener
{
    /// <summary>
    /// Provides a class that contains ready made setting constants.
    /// </summary>
    public class SettingConstants
    {
        /// <summary>
        /// The name of the setting that holds the log folder location.
        /// </summary>
        public const string LogFolder = "Codefarts.Logging.LogFolder";

        /// <summary>
        /// The name of the setting that holds the maximum size in bytes that a log file is allowed to grow to.
        /// </summary>
        public const string MaxLogFileSize = "Codefarts.Logging.MaxLogFileSize";
    }
}
