// <copyright>
//   Copyright (c) 2012 Codefarts
//   All rights reserved.
//   contact@codefarts.com
//   http://www.codefarts.com
// </copyright>

namespace Codefarts.Logging.XmlTraceListener
{
    using System;
    using System.ComponentModel.Composition;
    using System.Diagnostics;
    using System.IO;
    using System.Xml;
    using System.Xml.Serialization;

    using Codefarts.IdManager;
    using Codefarts.Settings;

    /// <summary>
    ///     Provides a <see cref="XmlSerializer" /> compatible xml trace listener for xml files.
    /// </summary>
    [Export(typeof(ILogging))]
    public class XmlTraceListener : TraceListener, ILogging
    {
        #region Fields

        /// <summary>The maximum log file size.</summary>
        private int maxLogFileSize = 1048576 * 25;

        /// <summary>
        ///     The xml writer used to save the data.
        /// </summary>
        private XmlTextWriter xmlWriter;

        private IdManager manager;

        /// <summary>The file stream where data gets stored.</summary>
        private FileStream stream;

        #endregion

        #region Constructors and Destructors

        /// <summary>Initializes a new instance of the <see cref="XmlTraceListener" /> class.</summary>
        public XmlTraceListener()
        {
            var size = 0;
            SettingsManager.Instance.TryGetSetting(SettingConstants.MaxLogFileSize, out size);
            if (size > 0)
            {
                this.MaxLogFileSize = size;
            }

            this.manager = new IdManager();
            this.StartNewLog();
        }

        #endregion

        #region Public Properties

        /// <summary>Gets or sets the maximum size of the log file in bytes.</summary>
        /// <value>The maximum size of the log file in bytes.</value>
        /// <exception cref="System.ArgumentOutOfRangeException">If value is less then 1024.</exception>
        public int MaxLogFileSize
        {
            get
            {
                return this.maxLogFileSize;
            }

            set
            {
                if (value < 1024)
                {
                    throw new ArgumentOutOfRangeException("value");
                }

                this.maxLogFileSize = value;
            }
        }

        #endregion

        #region Public Methods and Operators

        /// <summary>Releases all resources used by the <see cref="T:System.Diagnostics.TraceListener" />.</summary>
        public new void Dispose()
        {
            if (this.xmlWriter == null)
            {
                return;
            }

            lock (this.xmlWriter)
            {
                this.xmlWriter.WriteEndElement();
                try
                {
                    this.xmlWriter.Close();
                    this.stream.Close();
                }
                finally
                {
                    this.stream = null;
                    this.xmlWriter = null;
                }
            }
        }

        /// <summary>
        /// Emits an error message to the listener you create when you implement the
        ///     <see cref="T:System.Diagnostics.TraceListener"/> class.
        /// </summary>
        /// <param name="message">
        /// A message to emit.
        /// </param>
        public override void Fail(string message)
        {
            this.WriteElement(LogEntryType.Error, message, string.Empty);
        }

        /// <summary>
        /// Emits an error message and a detailed error message to the listener you create when you implement the
        ///     <see cref="T:System.Diagnostics.TraceListener"/> class.
        /// </summary>
        /// <param name="message">
        /// A message to emit.
        /// </param>
        /// <param name="detailMessage">
        /// A detailed message to emit.
        /// </param>
        public override void Fail(string message, string detailMessage)
        {
            this.WriteElement(LogEntryType.Error, message, detailMessage);
        }

        /// <summary>
        /// Logs a simple message to the log.
        /// </summary>
        /// <param name="message">
        /// The message to be logged.
        /// </param>
        public void Log(string message)
        {
            this.WriteElement(LogEntryType.Generic, message, null);
        }

        /// <summary>
        /// Logs a simple message to the log.
        /// </summary>
        /// <param name="type">
        /// The type of message to be logged.
        /// </param>
        /// <param name="message">
        /// The message to be logged.
        /// </param>
        public void Log(LogEntryType type, string message)
        {
            this.WriteElement(LogEntryType.Generic, message, null);
        }

        /// <summary>
        /// Logs a simple message to the log.
        /// </summary>
        /// <param name="message">
        /// The message to be logged.
        /// </param>
        /// <param name="category">
        /// The category associated with the log.
        /// </param>
        public void Log(string message, string category)
        {
            this.WriteElement(LogEntryType.Generic, message, category);
        }

        /// <summary>
        /// Logs a simple message to the log.
        /// </summary>
        /// <param name="type">
        /// The type of message to be logged.
        /// </param>
        /// <param name="message">
        /// The message to be logged.
        /// </param>
        /// <param name="category">
        /// The category associated with the log.
        /// </param>
        public void Log(LogEntryType type, string message, string category)
        {
            this.WriteElement(type, message, category);
        }

        /// <summary>
        /// When overridden in a derived class, writes the specified message to the listener you create in the derived
        ///     class.
        /// </summary>
        /// <param name="message">
        /// A message to write.
        /// </param>
        public override void Write(string message)
        {
            this.WriteElement(LogEntryType.Generic, message, string.Empty);
        }

        /// <summary>
        /// Writes the value of the object's <see cref="M:System.Object.ToString"/> method to the listener you create when you
        ///     implement the <see cref="T:System.Diagnostics.TraceListener"/> class.
        /// </summary>
        /// <param name="o">
        /// An <see cref="T:System.Object"/> whose fully qualified class name you want to write.
        /// </param>
        public override void Write(object o)
        {
            this.WriteElement(LogEntryType.Generic, o == null ? string.Empty : o.ToString(), string.Empty);
        }

        /// <summary>
        /// Writes a category name and the value of the object's <see cref="M:System.Object.ToString"/> method to the listener
        ///     you create when you implement the <see cref="T:System.Diagnostics.TraceListener"/> class.
        /// </summary>
        /// <param name="o">
        /// An <see cref="T:System.Object"/> whose fully qualified class name you want to write.
        /// </param>
        /// <param name="category">
        /// A category name used to organize the output.
        /// </param>
        public override void Write(object o, string category)
        {
            this.WriteElement(LogEntryType.Generic, o == null ? string.Empty : o.ToString(), category);
        }

        /// <summary>
        /// Writes a category name and a message to the listener you create when you implement the
        ///     <see cref="T:System.Diagnostics.TraceListener"/> class.
        /// </summary>
        /// <param name="message">
        /// A message to write.
        /// </param>
        /// <param name="category">
        /// A category name used to organize the output.
        /// </param>
        public override void Write(string message, string category)
        {
            this.WriteElement(LogEntryType.Generic, message, category);
        }

        /// <summary>
        /// When overridden in a derived class, writes a message to the listener you create in the derived class, followed by a
        ///     line terminator.
        /// </summary>
        /// <param name="message">
        /// A message to write.
        /// </param>
        public override void WriteLine(string message)
        {
            this.WriteElement(LogEntryType.Generic, message, string.Empty);
        }

        /// <summary>
        /// Writes a category name and a message to the listener you create when you implement the
        ///     <see cref="T:System.Diagnostics.TraceListener"/> class, followed by a line terminator.
        /// </summary>
        /// <param name="message">
        /// A message to write.
        /// </param>
        /// <param name="category">
        /// A category name used to organize the output.
        /// </param>
        public override void WriteLine(string message, string category)
        {
            this.WriteElement(LogEntryType.Generic, message, category);
        }

        /// <summary>
        /// Writes the value of the object's <see cref="M:System.Object.ToString"/> method to the listener you create when you
        ///     implement the <see cref="T:System.Diagnostics.TraceListener"/> class, followed by a line terminator.
        /// </summary>
        /// <param name="o">
        /// An <see cref="T:System.Object"/> whose fully qualified class name you want to write.
        /// </param>
        public override void WriteLine(object o)
        {
            this.WriteElement(LogEntryType.Generic, o == null ? string.Empty : o.ToString(), string.Empty);
        }

        /// <summary>
        /// Writes a category name and the value of the object's <see cref="M:System.Object.ToString"/> method to the listener
        ///     you create when you implement the <see cref="T:System.Diagnostics.TraceListener"/> class, followed by a line
        ///     terminator.
        /// </summary>
        /// <param name="o">
        /// An <see cref="T:System.Object"/> whose fully qualified class name you want to write.
        /// </param>
        /// <param name="category">
        /// A category name used to organize the output.
        /// </param>
        public override void WriteLine(object o, string category)
        {
            this.WriteElement(LogEntryType.Generic, o == null ? string.Empty : o.ToString(), category);
        }

        #endregion

        #region Methods

        /// <summary>
        /// Releases the unmanaged resources used by the <see cref="T:System.Diagnostics.TraceListener"/> and optionally
        ///     releases the managed resources.
        /// </summary>
        /// <param name="disposing">
        /// true to release both managed and unmanaged resources; false to release only unmanaged
        ///     resources.
        /// </param>
        protected override void Dispose(bool disposing)
        {
            base.Dispose(disposing);
            this.Dispose();
        }

        /// <summary>Starts a new log file.</summary>
        private void StartNewLog()
        {
            var dataFolder = SettingsManager.Instance.GetSetting(SettingConstants.LogFolder, string.Empty);
            var fileName = Path.Combine(dataFolder, string.Format("{0}.log", DateTime.Now.ToString(@"M-d-yyyy hh-mm-ss tt")));
            while (File.Exists(fileName))
            {
                fileName = Path.Combine(dataFolder, string.Format("{0}.log", DateTime.Now.ToString(@"M-d-yyyy hh-mm-ss tt")));
            }

            this.manager.Reset();

            // ensure log folder exists
            if (!string.IsNullOrWhiteSpace(fileName))
            {
                var path = Path.GetDirectoryName(fileName);
                if (!string.IsNullOrWhiteSpace(path))
                {
                    Directory.CreateDirectory(path);
                }
            }

            this.stream = File.Open(fileName, FileMode.Create, FileAccess.Write, FileShare.Read);

            this.xmlWriter = new XmlTextWriter(this.stream, null) { Formatting = Formatting.Indented, Indentation = 4 };
            this.xmlWriter.WriteStartDocument();
            this.xmlWriter.WriteStartElement("ArrayOfLogModel");

            this.xmlWriter.WriteAttributeString("xmlns", "xsi", null, "http://www.w3.org/2001/XMLSchema-instance");
            this.xmlWriter.WriteAttributeString("xmlns", "xsd", null, "http://www.w3.org/2001/XMLSchema");
        }

        /// <summary>
        /// Writes the trace/debug message to an XML file.
        /// </summary>
        /// <param name="type">
        /// The type.
        /// </param>
        /// <param name="message">
        /// The message to be written.
        /// </param>
        /// <param name="category">
        /// The category.
        /// </param>
        private void WriteElement(LogEntryType type, string message, string category)
        {
            lock (this.xmlWriter)
            {
                this.xmlWriter.WriteStartElement("LogModel");

                this.WriteWholeElement("Id", this.manager.NewId());
                this.WriteWholeElement("Message", message);
                this.WriteWholeElement("TimeStamp", DateTime.Now);
                this.WriteWholeElement("Type", type.ToString());
                this.WriteWholeElement("Category", category);

                this.xmlWriter.WriteEndElement();

                this.xmlWriter.Flush();
                if (this.xmlWriter.BaseStream.Length > this.maxLogFileSize)
                {
                    this.xmlWriter.WriteEndElement();
                    this.xmlWriter.Flush();
                    this.xmlWriter.Close();
                    this.StartNewLog();
                }
            }
        }

        /// <summary>
        /// Writes the whole element.
        /// </summary>
        /// <param name="name">
        /// The name of the element.
        /// </param>
        /// <param name="value">
        /// The value for the element.
        /// </param>
        private void WriteWholeElement(string name, object value)
        {
            if (value == null)
            {
                return;
            }

            this.xmlWriter.WriteStartElement(name);
            this.xmlWriter.WriteValue(value);
            this.xmlWriter.WriteEndElement();
        }

        #endregion
    }
}