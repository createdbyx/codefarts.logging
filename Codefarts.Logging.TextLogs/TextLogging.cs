﻿// <copyright>
//   Copyright (c) 2012 Codefarts
//   All rights reserved.
//   contact@codefarts.com
//   http://www.codefarts.com
// </copyright>

namespace Codefarts.Logging.TextLogs
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel;
    using System.ComponentModel.Composition;
    using System.IO;
    using System.Threading.Tasks;

    /// <summary>
    /// Provides a text based file based log.
    /// </summary>
    [Export(typeof(ILogging))]
    public class TextLogging : ILogging, INotifyPropertyChanged
    {
        #region Fields

        /// <summary>
        /// Holds a list of log entries.
        /// </summary>
        private readonly List<LogModel> model = new List<LogModel>();

        /// <summary>
        /// Holds the name of the active log filename.
        /// </summary>
        private string logFile;

        private string fileName;

        private string logFolder;

        private int maxLogFileSize = 1048576;

        #endregion

        #region Constructors and Destructors

        /// <summary>Initializes a new instance of the <see cref="TextLogging" /> class.</summary>
        public TextLogging(string logFolder)
        {
            if (!Directory.Exists(logFolder))
            {
                throw new DirectoryNotFoundException("Specified log folder directory does not exist.");
            }

            this.logFolder = logFolder;
            this.NewLogFile();
        }

        public TextLogging(string logFolder, int maxLogSize) : this(logFolder)
        {
            if (maxLogSize < 1)
            {
                throw new ArgumentOutOfRangeException("maxLogSize", "Must be greater then 0.");
            }

            this.maxLogFileSize = maxLogSize;
        }

        /// <summary>Finalizes an instance of the <see cref="TextLogging"/> class.</summary>
        ~TextLogging()
        {
            this.DoSave(true, true);
        }

        #endregion

        #region Public Methods and Operators

        /// <summary>
        /// Logs a simple message to the log.
        /// </summary>
        /// <param name="message">
        /// The message to be logged.
        /// </param>
        public void Log(string message)
        {
            this.Log(LogEntryType.Generic, message, null);
        }

        /// <summary>
        /// Logs a simple message to the log.
        /// </summary>
        /// <param name="type">
        /// The type of message to be logged.
        /// </param>
        /// <param name="message">
        /// The message to be logged.
        /// </param>
        public void Log(LogEntryType type, string message)
        {
            this.Log(type, message, null);
        }

        /// <summary>
        /// Logs a simple message to the log.
        /// </summary>
        /// <param name="message">
        /// The message to be logged.
        /// </param>
        /// <param name="category">
        /// The category associated with the log.
        /// </param>
        public void Log(string message, string category)
        {
            this.Log(LogEntryType.Generic, message, category);
        }

        /// <summary>
        /// Logs a simple message to the log.
        /// </summary>
        /// <param name="type">
        /// The type of message to be logged.
        /// </param>
        /// <param name="message">
        /// The message to be logged.
        /// </param>
        /// <param name="category">
        /// The category associated with the log.
        /// </param>
        public async void Log(LogEntryType type, string message, string category)
        {
            var model = new LogModel();
            model.Category = category;
            model.Message = message;
            model.TimeStamp = DateTime.Now;
            model.Type = type;
            model.Id = this.model.Count;

            this.model.Add(model);
            await this.DoSave(false, false);
        }

        #endregion

        #region Methods

        /// <summary>Saves the current list of log entries.</summary>
        /// <returns>Returns a Task object for asynchronous code.</returns>
        private async Task DoSave(bool flush, bool? finalizing)
        {
            //if (flush == false  && this.model.Count < 8192)
            //{
            //    return;
            //}

            if (string.IsNullOrWhiteSpace(this.fileName))
            {
                this.fileName = Path.Combine(this.LogFolder, this.logFile);
            }

            await FileStorageAdapter.SaveData(this.model, this.fileName, finalizing);

            if (File.Exists(this.fileName))
            {
                // check if existing file size beyond our 5Mb limit and start a new one if it is
                var info = new FileInfo(this.fileName);
                if (info.Length > this.maxLogFileSize)
                {
                    this.NewLogFile();
                    this.fileName = Path.Combine(Path.GetDirectoryName(this.fileName), this.logFile);
                    this.model.Clear();
                }
            }
        }

        public virtual int MaxLogFileSize
        {
            get
            {
                return this.maxLogFileSize;
            }

            set
            {
                if (this.maxLogFileSize != value && value > 0)
                {
                    this.maxLogFileSize = value;
                    this.OnPropertyChanged("MaxLogFileSize");
                }
            }
        }

        public virtual string LogFolder
        {
            get
            {
                return this.logFolder;
            }

            set
            {
                if (Directory.Exists(value) && this.logFolder != value)
                {
                    this.logFolder = value;
                    this.OnPropertyChanged("LogFolder");
                }
            }
        }

        /// <summary>
        ///     Starts a new log file name.
        /// </summary>
        private void NewLogFile()
        {
            this.logFile = string.Format("{0}.log", DateTime.Now.ToString(@"M-d-yyyy hh-mm-ss tt"));
        }

        #endregion

        public event PropertyChangedEventHandler PropertyChanged;

        protected virtual void OnPropertyChanged(string propertyName)
        {
            var handler = this.PropertyChanged;
            if (handler != null)
            {
                handler(this, new PropertyChangedEventArgs(propertyName));
            }
        }
    }
}