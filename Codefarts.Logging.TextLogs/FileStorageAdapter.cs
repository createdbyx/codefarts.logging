﻿// <copyright>
//   Copyright (c) 2012 Codefarts
//   All rights reserved.
//   contact@codefarts.com
//   http://www.codefarts.com
// </copyright>         

namespace Codefarts.Logging.TextLogs
{
    using System;
    using System.Collections.Generic;
    using System.Diagnostics;
    using System.IO;
    using System.Linq;
    using System.Threading;
    using System.Threading.Tasks;
    using System.Xml.Serialization;

    /// <summary>
    /// The file storage adapter.
    /// </summary>
    public static class FileStorageAdapter
    {
        #region Static Fields

        /// <summary>Holds a dictionary of semaphores to assist with threading.</summary>
        private static readonly Dictionary<string, SemaphoreSlim> Semaphores = new Dictionary<string, SemaphoreSlim>();

        #endregion

        #region Public Methods and Operators

        /// <summary>
        /// Saves the data to a file.
        /// </summary>
        /// <param name="data">
        /// The data to be serialized.
        /// </param>
        /// <param name="filename">
        /// The filename where the data will be saved to.
        /// </param>
        /// <returns>
        /// Returns a Task object for asynchronous code.
        /// </returns>
        public static async Task SaveData(List<LogModel> data, string filename, bool? finalizing)
        {
            var semaphore = GetSemaphore(filename);
            await semaphore.WaitAsync();

            try
            {
                Debug.Indent();
                Debug.WriteLine("Textlogging SaveData start ");
                var folder = Path.GetDirectoryName(filename);
                Directory.CreateDirectory(folder);
                using (var storageFile = new FileStream(filename, FileMode.OpenOrCreate, FileAccess.Write, FileShare.None, 8196))
                {
                    using (var writer = new StreamWriter(storageFile))
                    {
                        await writer.WriteAsync(string.Join("\r\n", data.Select(x => string.Format("{0} ({1}) [{2}] {3}: {4}", x.TimeStamp, x.Id, x.Category, x.Type, x.Message))));
                    }

                    Debug.WriteLine("Textlogging SaveData length " + storageFile.Length);
                }

                Debug.WriteLine("Textlogging SaveData complete ");
            }
            catch (Exception ex)
            {
                Debug.WriteLine("Textlogging SaveData exception caught ");
                Debug.WriteLine(ex.Message);
            }
            finally
            {
                Debug.Unindent();
                semaphore.Release();
            }
        }

        #endregion

        #region Methods

        /// <summary>
        /// Gets the semaphore for a given path and filename.
        /// </summary>
        /// <param name="filename">
        /// The filename to retrieve the semaphore for.
        /// </param>
        /// <returns>
        /// Returns a semaphore object associated with the filename.
        /// </returns>
        internal static SemaphoreSlim GetSemaphore(string filename)
        {
            if (Semaphores.ContainsKey(filename))
            {
                return Semaphores[filename];
            }

            var semaphore = new SemaphoreSlim(1);
            Semaphores[filename] = semaphore;
            return semaphore;
        }

        #endregion
    }
}