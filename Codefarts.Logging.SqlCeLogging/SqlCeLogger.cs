﻿// <copyright>
//   Copyright (c) 2012 Codefarts
//   All rights reserved.
//   contact@codefarts.com
//   http://www.codefarts.com
// </copyright>

namespace Codefarts.Logging.SqlCeLogging
{
    using System;
    using System.ComponentModel.Composition;
    using System.IO;

    using Codefarts.Settings;

    [Export(typeof(ILogging))]
    public class SqlCeLogger : ILogging
    {
        private string logFileName;

        public SqlCeLogger()
        {
            var folder = SettingsManager.Instance.GetSetting(SettingConstants.LogFolder, string.Empty);
            this.logFileName = Path.Combine(folder, "Logs.sdf");
        }

        public void Log(string message)
        {
            this.Log(LogEntryType.Generic, message, null);
        }

        public void Log(LogEntryType type, string message)
        {
            this.Log(type, message, null);
        }

        public void Log(string message, string category)
        {
            this.Log(LogEntryType.Generic, message, category);
        }

        public async void Log(LogEntryType type, string message, string category)
        {
            var context = new LoggingContext(this.logFileName);

            context.LogEntries.Add(new LogModel()
                                       {
                                           Category = category,
                                           Message = message,
                                           TimeStamp = DateTime.Now,
                                           Type = type
                                       });
            await context.SaveChangesAsync();
        }
    }
}
