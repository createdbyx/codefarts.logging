﻿// <copyright>
//   Copyright (c) 2012 Codefarts
//   All rights reserved.
//   contact@codefarts.com
//   http://www.codefarts.com
// </copyright>

namespace Codefarts.Logging.SqlCeLogging
{
    using System;
    using System.Data.Entity;
    using System.Data.Entity.Infrastructure;
    using System.Data.Entity.SqlServerCompact;

    using Codefarts.Settings;

    public class LoggingDbConfiguration : DbConfiguration
    {        
        public LoggingDbConfiguration()
        {
            var path = SettingsManager.Instance.GetSetting(SettingConstants.LogFolder, string.Empty);
            if (string.IsNullOrWhiteSpace(path))
            {
                path = Environment.CurrentDirectory;
            }

            var sqlCeConnectionFactory = new SqlCeConnectionFactory(
                SqlCeProviderServices.ProviderInvariantName,
                path,
                string.Empty);
            this.SetDefaultConnectionFactory(sqlCeConnectionFactory);
            this.SetProviderServices(SqlCeProviderServices.ProviderInvariantName, SqlCeProviderServices.Instance);
        }
    }
}