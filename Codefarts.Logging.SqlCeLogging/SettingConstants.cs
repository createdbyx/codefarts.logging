﻿// <copyright>
//   Copyright (c) 2012 Codefarts
//   All rights reserved.
//   contact@codefarts.com
//   http://www.codefarts.com
// </copyright>

namespace Codefarts.Logging.SqlCeLogging
{
    public class SettingConstants
    {
        public const string LogFolder = "Codefarts.Logging.LogFolder";
    }
}
