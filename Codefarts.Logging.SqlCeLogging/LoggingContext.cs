﻿// <copyright>
//   Copyright (c) 2012 Codefarts
//   All rights reserved.
//   contact@codefarts.com
//   http://www.codefarts.com
// </copyright>

namespace Codefarts.Logging.SqlCeLogging
{
    using System.Data.Entity;

    [DbConfigurationType(typeof(LoggingDbConfiguration))]
    public class LoggingContext : DbContext
    {
        public LoggingContext(string filename)
            : base(filename)
        {
        }

        public DbSet<LogModel> LogEntries { get; set; }
    }
}