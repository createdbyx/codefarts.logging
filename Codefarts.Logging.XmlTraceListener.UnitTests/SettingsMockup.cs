﻿// <copyright>
//   Copyright (c) 2012 Codefarts
//   All rights reserved.
//   contact@codefarts.com
//   http://www.codefarts.com
// </copyright>

namespace Codefarts.Logging.XmlTraceListener.UnitTests
{
    using System;
    using System.Collections.Generic;

    using Codefarts.Settings;

    public class SettingsMockup : IValues<string>
    {
        public SettingsMockup()
        {
            this.HasLogFolderSetting = true;
        }

        public event EventHandler<ValueChangedEventArgs<string>> ValueChanged;
        public string Path { get; set; }
        public int MaxLogFileSize { get; set; }
        public bool HasLogFolderSetting { get; set; }

        public T GetValue<T>(string key)
        {
            switch (key)
            {
                case SettingConstants.LogFolder:
                    if (this.HasLogFolderSetting)
                    {
                        return (T)Convert.ChangeType(this.Path, typeof(T));
                    }

                    break;

                case SettingConstants.MaxLogFileSize:
                    return (T)Convert.ChangeType(this.MaxLogFileSize, typeof(T));
            }

            throw new ArgumentException("Bad key given!", "key");
        }

        public bool HasValue(string name)
        {
            switch (name)
            {
                case SettingConstants.LogFolder:
                    return this.HasLogFolderSetting;

                case SettingConstants.MaxLogFileSize:
                    return true;
            }

            return false;
        }

        public void RemoveValue(string name)
        {
            // do nothing
        }

        public void SetValue(string name, object value)
        {
            // do nothing
        }

        public string[] GetValueKeys()
        {
            var values= new List<string>(new[] { SettingConstants.MaxLogFileSize });
            if (this.HasLogFolderSetting)
            {
                values.Add(SettingConstants.LogFolder);
            }
            return values.ToArray();
        }
    }
}
