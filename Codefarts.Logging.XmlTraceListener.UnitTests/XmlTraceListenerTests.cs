﻿// <copyright>
//   Copyright (c) 2012 Codefarts
//   All rights reserved.
//   contact@codefarts.com
//   http://www.codefarts.com
// </copyright>

namespace Codefarts.Logging.XmlTraceListener.UnitTests
{
    using System;
    using System.Collections.Generic;
    using System.IO;
    using System.Linq;
    using System.Threading;
    using System.Threading.Tasks;
    using System.Xml.Serialization;

    using Codefarts.Settings;

    using Microsoft.VisualStudio.TestTools.UnitTesting;

    /// <summary>
    /// The xml logs tests.
    /// </summary>
    [TestClass]
    public class XmlTraceListenerTests
    {
        /// <summary>
        /// The folder.
        /// </summary>
        private string folder;

        /// <summary>
        /// The startup.
        /// </summary>
        [TestInitialize]
        public void Startup()
        {
            this.folder = Path.Combine(Environment.CurrentDirectory, "XmlLogTests");

            this.CleanupFolder();

            // ensure folder exists
            Directory.CreateDirectory(this.folder);

            SettingsManager.Instance.Values = new SettingsMockup() { Path = this.folder, MaxLogFileSize = 1048576 * 5 };
        }

        /// <summary>
        /// The cleanup folder.
        /// </summary>
        private void CleanupFolder()
        {
            if (Directory.Exists(this.folder))
            {
                Directory.Delete(this.folder, true);
            }

            var files = Directory.GetFiles(Environment.CurrentDirectory, "*.log");
            foreach (var file in files)
            {
                File.Delete(file);
            }
        }

        /// <summary>
        /// The shut down.
        /// </summary>
        [TestCleanup]
        public void ShutDown()
        {
            this.CleanupFolder();
        }

        /// <summary>
        /// The constructor.
        /// </summary>
        [TestMethod]
        public void Constructor()
        {
            XmlTraceListener value = null;
            try
            {
                value = new XmlTraceListener();
            }
            catch (Exception ex)
            {
                Assert.Fail("Threw unexpected exception in constructor!");
            }

            // call dispose to enforce closure and cleanup of the xml log file
            ((IDisposable)value).Dispose();
            value = null;

            // collect to invoke finalizer on XmlTraceListener
            GC.Collect();
            Thread.Sleep(1000);

            var files = Directory.GetFiles(this.folder, "*.log");
            Assert.IsTrue(files.Length == 1, "Expected only one log file!");
            Assert.IsTrue(new FileInfo(files[0]).Length > 125, "Expected log file to contain data!");
        }

        /// <summary>
        /// The constructor.
        /// </summary>
        [TestMethod]
        public void MissingSettingLogFolder()
        {
            var settings = SettingsManager.Instance.Values as SettingsMockup;
            settings.HasLogFolderSetting = false;
            XmlTraceListener value = null;
            var files = Directory.GetFiles(Environment.CurrentDirectory, "*.log");
            Assert.IsTrue(files.Length == 0, string.Format("Test cannot be performed because there are already files in \"{0}\"!", Environment.CurrentDirectory));

            try
            {
                value = new XmlTraceListener();
            }
            catch (Exception ex)
            {
                Assert.Fail("Threw unexpected exception in constructor!");
            }

            // call dispose to enforce closure and cleanup of the xml log file
            ((IDisposable)value).Dispose();
            value = null;

            // collect to invoke finalizer on XmlTraceListener
            GC.Collect();
            Thread.Sleep(1000);

            files = Directory.GetFiles(Environment.CurrentDirectory, "*.log");
            Assert.IsTrue(files.Length == 1, "Expected only one log file!");
            Assert.IsTrue(new FileInfo(files[0]).Length > 125, "Expected log file to contain data!");
        }

        /// <summary>
        /// The constructor.
        /// </summary>
        [TestMethod]
        public void MissingLogFolder()
        {
            var settings = SettingsManager.Instance.Values as SettingsMockup;
            settings.Path = Path.Combine(settings.Path, "Test");
            XmlTraceListener value = null;
            Assert.IsFalse(Directory.Exists(settings.Path), string.Format("Test cannot be performed because it is in a invalid state. The Tests folder already exists! \"{0}\"!", settings.Path));

            try
            {
                value = new XmlTraceListener();
            }
            catch (Exception ex)
            {
                Assert.Fail("Threw unexpected exception in constructor!");
            }

            // call dispose to enforce closure and cleanup of the xml log file
            ((IDisposable)value).Dispose();
            value = null;

            // collect to invoke finalizer on XmlTraceListener
            GC.Collect();
            Thread.Sleep(1000);

            Assert.IsTrue(Directory.Exists(settings.Path), string.Format("Expected log folder to exist! \"{0}\"", settings.Path));

            var files = Directory.GetFiles(settings.Path, "*.log");
            Assert.IsTrue(files.Length == 1, "Expected only one log file!");
            Assert.IsTrue(new FileInfo(files[0]).Length > 125, "Expected log file to contain data!");
        }

        /// <summary>
        /// The just message.
        /// </summary>
        [TestMethod]
        public void JustMessage()
        {
            try
            {
                Logging.Repositories.Add(new XmlTraceListener());
                Logging.Log("Test Message");
            }
            catch (Exception ex)
            {
                Assert.Fail("Threw unexpected exception!");
            }

            // call dispose to enforce closure and cleanup of the xml log file
            ((IDisposable)Logging.Repositories[0]).Dispose();
            Logging.Repositories[0] = null;
            Logging.Repositories.RemoveAt(0);

            // collect to invoke finalizer on XmlTraceListener
            GC.Collect();
            Thread.Sleep(1000);

            var files = Directory.GetFiles(this.folder, "*.log");
            Assert.IsTrue(files.Length == 1, "Expected only one log file!");
            Assert.IsTrue(new FileInfo(files[0]).Length > 150, "Expected log file to contain data!");
        }

        /// <summary>
        /// The bulk messages.
        /// </summary>
        [TestMethod]
        public void BulkMessages()
        {
            try
            {
                Logging.Repositories.Add(new XmlTraceListener());
                for (var i = 0; i < 50000; i++)
                {
                    Logging.Log(string.Format("{0}: Test message", i));
                }
            }
            catch (Exception ex)
            {
                Assert.Fail("Threw unexpected exception!");
            }

            // call dispose to enforce closure and cleanup of the xml log file
            ((IDisposable)Logging.Repositories[0]).Dispose();
            Logging.Repositories[0] = null;
            Logging.Repositories.RemoveAt(0);

            // collect to invoke finalizer on XmlTraceListener
            GC.Collect();
            Thread.Sleep(1000);

            var files = Directory.GetFiles(this.folder, "*.log");
            Assert.IsTrue(files.Length == 2, "Expected two log files!");
            Assert.IsTrue(new FileInfo(files[0]).Length > 150, "Expected log file to contain data!");
            Assert.IsTrue(new FileInfo(files[1]).Length > 150, "Expected log file to contain data!");
        }

        [TestMethod]
        public void MultithreadedWithTasks()
        {
            Logging.Repositories.Add(new XmlTraceListener());

            var taskCount = 10000;
            var taskList = new Task[taskCount];
            var idValue = -1;
            for (var i = 0; i < taskList.Length; i++)
            {
                taskList[i] = Task.Factory.StartNew(() => Logging.Log(string.Format("{0}: Test message", Interlocked.Increment(ref idValue))));
            }

            Task.WaitAll(taskList);

            Assert.AreEqual(taskCount - 1, idValue);
            var files = Directory.GetFiles(this.folder, "*.log");
            Assert.IsTrue(files.Length == 1, "Expected 1 log file!");
            Assert.IsTrue(new FileInfo(files[0]).Length > 150, "Expected log file to contain data!");

            // call dispose to enforce closure and cleanup of the xml log file
            ((IDisposable)Logging.Repositories[0]).Dispose();
            Logging.Repositories[0] = null;
            Logging.Repositories.RemoveAt(0);

            // collect to invoke finalizer on XmlTraceListener
            GC.Collect();
            Thread.Sleep(1000);

            // de-serialize log
            List<LogModel> data;
            using (var storageFile = new FileStream(files[0], FileMode.Open, FileAccess.Read, FileShare.None, 8196))
            {
                var serializer = new XmlSerializer(typeof(List<LogModel>));
                data = (List<LogModel>)serializer.Deserialize(storageFile);
            }

            Assert.AreEqual(taskCount, data.Count, "Log count does not contain the same number of tasks that were invoked.");

            // check if all sequence numbers are present
            var orderedMessages = data.OrderBy(x => int.Parse(x.Message.Substring(0, x.Message.IndexOf(":")))).Select(x => x.Message).ToArray();
            var orderedById = data.OrderBy(x => x.Id).ToArray();
            for (var i = 0; i < orderedMessages.Length; i++)
            {
                Assert.AreEqual(string.Format("{0}: Test message", i), orderedMessages[i], "Deserialized message does not match.");
                Assert.AreEqual(i, orderedById[i].Id, "Id's does not match.");
            }
        }

        [TestMethod]
        public void DeserializeMessages()
        {
            try
            {
                Logging.Repositories.Add(new XmlTraceListener());
                Logging.Log("Test Message");
            }
            catch (Exception ex)
            {
                Assert.Fail("Threw unexpected exception!");
            }
            var timeValue = DateTime.Now;

            // call dispose to enforce closure and cleanup of the xml log file
            ((IDisposable)Logging.Repositories[0]).Dispose();
            Logging.Repositories[0] = null;
            Logging.Repositories.RemoveAt(0);

            // collect to invoke finalizer on XmlTraceListener
            GC.Collect();
            Thread.Sleep(1000);

            // de-serialize log
            List<LogModel> data;
            var files = Directory.GetFiles(this.folder, "*.log");
            using (var storageFile = new FileStream(files[0], FileMode.Open, FileAccess.Read, FileShare.None, 8196))
            {
                var serializer = new XmlSerializer(typeof(List<LogModel>));
                data = (List<LogModel>)serializer.Deserialize(storageFile);
            }

            Assert.AreEqual(1, data.Count, "Deserialized data contains more then one item.");
            Assert.AreEqual(0, data[0].Id, "Deserialized id does not match.");
            Assert.IsNull(null, data[0].Category, "Deserialized category does not match.");
            Assert.AreEqual("Test Message", data[0].Message, "Deserialized message does not match.");
            Assert.AreEqual(LogEntryType.Generic, data[0].Type, "Deserialized type does not match.");

            // check that the time for the log entry is within 10ms if the time we captured after making the log
            Assert.IsFalse(data[0].TimeStamp < timeValue - TimeSpan.FromMilliseconds(10), "Deserialized time is not in range.");
        }
    }
}
