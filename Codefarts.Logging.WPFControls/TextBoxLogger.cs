// <copyright file="TextBoxLogger.cs" company="Codefarts">
// Copyright (c) Codefarts
// contact@codefarts.com
// http://www.codefarts.com
// </copyright>

namespace Codefarts.Logging.WPFControls
{
    using System;
    using System.Windows.Controls;

    public class TextBoxLogger : ILogging
    {
        /// <summary>
        /// Initializes a new instance of the <see cref="TextBoxLogger"/> class.
        /// </summary>
        /// <param name="textBox">
        /// The text box.
        /// </param>
        public TextBoxLogger(TextBox textBox)
        {
            this.TextBox = textBox;
        }

        /// <summary>
        /// Initializes a new instance of the <see cref="TextBoxLogger"/> class.
        /// </summary>
        public TextBoxLogger()
        {
        }

        public bool AutoScroll
        {
            get; set;
        }

        public TextBox TextBox
        {
            get; set;
        }

        public void Log(string message)
        {
            this.Log(LogEntryType.Generic, message, null);
        }

        public void Log(LogEntryType type, string message)
        {
            this.Log(type, message, null);
        }

        public void Log(string message, string category)
        {
            this.Log(LogEntryType.Generic, message, category);
        }

        public void Log(LogEntryType type, string message, string category)
        {
            var textBox = this.TextBox;
            if (textBox != null)
            {
                var text = string.Format("\r\n{0}: ({1}) {2} -> {3}", DateTime.Now, category, type, message);
                textBox.Dispatcher.BeginInvoke(
                    (Action)(() =>
                        {
                            textBox.AppendText(text);
                            if (this.AutoScroll)
                            {
                                textBox.ScrollToEnd();
                            }
                        }));
            }
        }
    }
}