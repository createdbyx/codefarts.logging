﻿// <copyright file="RichTextBoxLogger.cs" company="Codefarts">
// Copyright (c) Codefarts
// contact@codefarts.com
// http://www.codefarts.com
// </copyright>

namespace Codefarts.Logging.WPFControls
{
    using System;
    using System.Windows.Controls;
    using System.Windows.Documents;
    using System.Windows.Media;

    using Codefarts.Logging;

    public class RichTextBoxLogger : ILogging
    {
        /// <summary>
        /// Initializes a new instance of the <see cref="RichTextBoxLogger"/> class.
        /// </summary>
        /// <param name="textBox">
        /// The text box.
        /// </param>
        public RichTextBoxLogger(RichTextBox textBox)
        {
            this.TextBox = textBox;
        }

        /// <summary>
        /// Initializes a new instance of the <see cref="RichTextBoxLogger"/> class.
        /// </summary>
        public RichTextBoxLogger()
        {
        }

        public bool AutoScroll
        {
            get; set;
        }

        public RichTextBox TextBox
        {
            get; set;
        }

        public void Log(string message)
        {
            this.Log(LogEntryType.Generic, message, null);
        }

        public void Log(LogEntryType type, string message)
        {
            this.Log(type, message, null);
        }

        public void Log(string message, string category)
        {
            this.Log(LogEntryType.Generic, message, category);
        }

        public void Log(LogEntryType type, string message, string category)
        {
            var textBox = this.TextBox;
            if (textBox != null)
            {
                //var text = string.Format("{0}: ({1}) {2} -> {3}\r\n", DateTime.Now, category, type, message);
                textBox.Dispatcher.BeginInvoke(
                        (Action)(() =>
                            {
                                var color = this.MessageBrush ?? textBox.Foreground;
                                switch (type)
                                {
                                    case LogEntryType.Information:
                                        color = this.InformationBrush == null ? color : this.InformationBrush;
                                        break;
                                    case LogEntryType.Warning:
                                        color = this.WarningBrush == null ? color : this.WarningBrush;
                                        break;
                                    case LogEntryType.Fail:
                                        color = this.FailBrush == null ? color : this.FailBrush;
                                        break;
                                    case LogEntryType.Event:
                                        color = this.EventBrush == null ? color : this.EventBrush;
                                        break;
                                    case LogEntryType.Generic:
                                        color = this.GenericBrush == null ? color : this.GenericBrush;
                                        break;
                                    case LogEntryType.Error:
                                        color = this.ErrorBrush == null ? color : this.ErrorBrush;
                                        break;
                                }

                                var tr = new Run(DateTime.Now.ToString(), textBox.Document.ContentEnd) { Foreground = this.DateTimeBrush ?? textBox.Foreground };
                                tr = new Run(": (", textBox.Document.ContentEnd);
                                tr = new Run(category, textBox.Document.ContentEnd) { Foreground = this.CategoryBrush ?? textBox.Foreground };
                                tr = new Run(") ", textBox.Document.ContentEnd);
                                tr = new Run(type.ToString(), textBox.Document.ContentEnd) { Foreground = this.TypeBrush ?? textBox.Foreground };
                                tr = new Run(" -> ", textBox.Document.ContentEnd);
                                tr = new Run(message, textBox.Document.ContentEnd) { Foreground = this.MessageBrush ?? color };
                                tr = new Run("\r\n", textBox.Document.ContentEnd);

                                if (this.AutoScroll && !textBox.IsSelectionActive)
                                {
                                    textBox.CaretPosition = textBox.Document.ContentEnd;
                                    textBox.ScrollToEnd();
                                }
                            }));
            }
        }

        public Brush InformationBrush
        {
            get; set;
        }

        public Brush WarningBrush
        {
            get; set;
        }

        public Brush FailBrush
        {
            get; set;
        }

        public Brush EventBrush
        {
            get; set;
        }

        public Brush GenericBrush
        {
            get; set;
        }

        public Brush ErrorBrush
        {
            get; set;
        }

        public Brush CategoryBrush
        {
            get; set;
        }

        public Brush DateTimeBrush
        {
            get; set;
        }

        public Brush TypeBrush
        {
            get; set;
        }

        public Brush MessageBrush
        {
            get; set;
        }
    }
}