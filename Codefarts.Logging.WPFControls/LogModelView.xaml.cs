﻿// <copyright file="LogModelView.xaml.cs" company="Codefarts">
// Copyright (c) Codefarts
// contact@codefarts.com
// http://www.codefarts.com
// </copyright>

namespace Codefarts.Logging.WPFControls
{
    using System.Windows.Controls;

    /// <summary>
    /// Interaction logic for LogModelView.xaml.
    /// </summary>
    public partial class LogModelView : UserControl
    {
        /// <summary>
        /// Initializes a new instance of the <see cref="LogModelView"/> class.
        /// </summary>
        public LogModelView()
        {
            this.InitializeComponent();
        }
    }
}
