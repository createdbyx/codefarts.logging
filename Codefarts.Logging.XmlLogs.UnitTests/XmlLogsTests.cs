﻿// --------------------------------------------------------------------------------------------------------------------
// <copyright company="" file="XmlLogsTests.cs">
//   
// </copyright>
// <summary>
//   The xml logs tests.
// </summary>
// --------------------------------------------------------------------------------------------------------------------

namespace Codefarts.Logging.XmlLogs.UnitTests
{
    using System;
    using System.IO;
    using System.Threading;
    using System.Threading.Tasks;

    using Codefarts.Settings;

    using Microsoft.VisualStudio.TestTools.UnitTesting;

    /// <summary>
    /// The xml logs tests.
    /// </summary>
    [TestClass]
    public class XmlLogsTests
    {
        /// <summary>
        /// The folder.
        /// </summary>
        private string folder;

        /// <summary>
        /// The startup.
        /// </summary>
        [TestInitialize]
        public void Startup()
        {
            this.folder = Path.Combine(Environment.CurrentDirectory, "XmlLogTests");

            this.CleanupFolder();

            // ensure folder exists
            Directory.CreateDirectory(this.folder);

            SettingsManager.Instance.Values = new SettingsMockup() { Path = this.folder };
        }

        /// <summary>
        /// The cleanup folder.
        /// </summary>
        private void CleanupFolder()
        {
            if (Directory.Exists(this.folder))
            {
                Directory.Delete(this.folder, true);
            }
        }

        /// <summary>
        /// The shut down.
        /// </summary>
        [TestCleanup]
        public void ShutDown()
        {
            this.CleanupFolder();
        }

        /// <summary>
        /// The constructor.
        /// </summary>
        [TestMethod]
        public void Constructor()
        {
            try
            {
                var value = new XmlLogging();
            }
            catch (Exception ex)
            {
                Assert.Fail("Threw unexpected exception in constructor!");
            }
        }

        /// <summary>
        /// The just message.
        /// </summary>
        [TestMethod]
        public void JustMessage()
        {
            try
            {
                Logging.Repositories.Add(new XmlLogging());
                Logging.Log("Test Message");
            }
            catch (Exception ex)
            {
                Assert.Fail("Threw unexpected exception!");
            }

            // call dispose to enforce closure and cleanup of the xml log file
            ((IDisposable)Logging.Repositories[0]).Dispose();
            Logging.Repositories[0] = null;
            Logging.Repositories.RemoveAt(0);

            // collect to invoke finalizer on XmlTraceListener
            GC.Collect();
            Thread.Sleep(1000);

            var files = Directory.GetFiles(this.folder, "*.log");
            Assert.IsTrue(files.Length == 1, "Expected only one log file!");
            Assert.IsTrue(new FileInfo(files[0]).Length > 150, "Expected log file to contain data!");
        }

        /// <summary>
        /// The bulk messages.
        /// </summary>
        [TestMethod]
        public void BulkMessages()
        {
            try
            {
                Logging.Repositories.Add(new XmlLogging());
                for (var i = 0; i < 100000; i++)
                {
                    Logging.Log(string.Format("Test Message: {0}", i));
                }
            }
            catch (Exception ex)
            {
                Assert.Fail("Threw unexpected exception!");
            }

            // call dispose to enforce closure and cleanup of the xml log file
            ((IDisposable)Logging.Repositories[0]).Dispose();
            Logging.Repositories[0] = null;
            Logging.Repositories.RemoveAt(0);

            // collect to invoke finalizer on XmlTraceListener
            GC.Collect();
            Thread.Sleep(1000);

            var files = Directory.GetFiles(this.folder, "*.log");
            Assert.IsTrue(files.Length == 2, "Expected two log files!");
            Assert.IsTrue(new FileInfo(files[0]).Length > 150, "Expected log file to contain data!");
            Assert.IsTrue(new FileInfo(files[1]).Length > 150, "Expected log file to contain data!");
        }
    }
}
