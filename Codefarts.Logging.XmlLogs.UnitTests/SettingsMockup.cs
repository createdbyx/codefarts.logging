﻿// <copyright>
//   Copyright (c) 2012 Codefarts
//   All rights reserved.
//   contact@codefarts.com
//   http://www.codefarts.com
// </copyright>

namespace Codefarts.Logging.XmlLogs.UnitTests
{
    using System;

    using Codefarts.Settings;

    public class SettingsMockup : IValues<string>
    {
        public event EventHandler<ValueChangedEventArgs<string>> ValueChanged;
        public string Path { get; set; }

        public T GetValue<T>(string key)
        {
            switch (key)
            {
                case SettingConstants.LogFolder:
                    return (T)Convert.ChangeType(this.Path, typeof(T));
            }

            throw new ArgumentException("Bad key given!", "key");
        }

        public bool HasValue(string name)
        {
            switch (name)
            {
                case SettingConstants.LogFolder:
                    return true;
            }

            return false;
        }

        public void RemoveValue(string name)
        {
            // do nothing
        }

        public void SetValue(string name, object value)
        {
            // do nothing
        }

        public string[] GetValueKeys()
        {
            return new[] { SettingConstants.LogFolder };
        }
    }
}
