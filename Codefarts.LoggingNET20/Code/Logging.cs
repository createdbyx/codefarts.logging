﻿// <copyright>
//   Copyright (c) 2012 Codefarts
//   All rights reserved.
//   contact@codefarts.com
//   http://www.codefarts.com
// </copyright>

namespace Codefarts.Logging
{
    using System;
    using System.Collections.Generic;

    /// <summary>
    ///     Provides a logging class.
    /// </summary>
    public class Logging
    {
        #region Static Fields

        /// <summary>
        ///     Holds a reference to a list of registered repositories.
        /// </summary>
        private static readonly List<ILogging> RepositoryList = new List<ILogging>();

        /// <summary>The lock object.</summary>
        private static readonly object lockObject = new object();

        #endregion

        #region Public Events

        /// <summary>Occurs when [logged entry].</summary>
        public static event EventHandler<LogModelEventArgs> LoggedEntry;

        #endregion

        #region Public Properties

        /// <summary>
        ///     Gets a list of registered logging repositories.
        /// </summary>
        public static List<ILogging> Repositories
        {
            get
            {
                return RepositoryList;
            }
        }

        #endregion

        #region Public Methods and Operators

        /// <summary>
        /// Logs a simple message to the log.
        /// </summary>
        /// <param name="message">
        /// The message to be logged.
        /// </param>
        /// <remarks>
        /// Logs will be logged with <see cref="LogEntryType.Generic"/>.
        /// </remarks>
        public static void Log(string message)
        {
            lock (lockObject)
            {
                foreach (var repository in RepositoryList)
                {
                    repository.Log(LogEntryType.Generic, message);
                }

                OnLoggedEntry(new LogModelEventArgs(LogEntryType.Generic, null, message));
            }
        }

        /// <summary>
        /// Logs a simple message to the log.
        /// </summary>
        /// <param name="type">
        /// The type of message to be logged.
        /// </param>
        /// <param name="message">
        /// The message to be logged.
        /// </param>
        public static void Log(LogEntryType type, string message)
        {
            lock (lockObject)
            {
                foreach (var repository in RepositoryList)
                {
                    repository.Log(type, message);
                }

                OnLoggedEntry(new LogModelEventArgs(type, null, message));
            }
        }

        /// <summary>
        /// Logs a simple message to the log.
        /// </summary>
        /// <param name="message">
        /// The message to be logged.
        /// </param>
        /// <param name="category">
        /// The category associated with the log.
        /// </param>
        /// <remarks>
        /// Logs will be logged with <see cref="LogEntryType.Generic"/>.
        /// </remarks>
        public static void Log(string message, string category)
        {
            lock (lockObject)
            {
                foreach (var repository in RepositoryList)
                {
                    repository.Log(LogEntryType.Generic, message, category);
                }

                OnLoggedEntry(new LogModelEventArgs(LogEntryType.Generic, category, message));
            }
        }

        /// <summary>
        /// Logs a simple message to the log.
        /// </summary>
        /// <param name="type">
        /// The type of message to be logged.
        /// </param>
        /// <param name="message">
        /// The message to be logged.
        /// </param>
        /// <param name="category">
        /// The category associated with the log.
        /// </param>
        public static void Log(LogEntryType type, string message, string category)
        {
            lock (lockObject)
            {
                foreach (var repository in RepositoryList)
                {
                    repository.Log(type, message, category);
                }

                OnLoggedEntry(new LogModelEventArgs(type, message, category));
            }
        }

        #endregion

        #region Methods

        /// <summary>
        /// Raises the <see cref="E:LoggedEntry"/> event.
        /// </summary>
        /// <param name="e">
        /// The <see cref="LogModelEventArgs"/> instance containing the event data.
        /// </param>
        protected static void OnLoggedEntry(LogModelEventArgs e)
        {
            var handler = LoggedEntry;
            if (handler != null)
            {
                handler(null, e);
            }
        }

        #endregion
    }
}